#needsrootforbuild

Name: bcache-tools
Version: 1.1
Release: 4
Summary: userspace tools for bcache
License: GPL-2.0
URL: http://bcache.evilpiepirate.org/
Source0: https://git.kernel.org/pub/scm/linux/kernel/git/colyli/bcache-tools.git/snapshot/%{name}-%{version}.tar.gz
Patch0: 0001-bcache-tools-Export-CACHED_UUID-and-CACHED_LABEL.patch

BuildRequires: gcc, tar, pkgconfig
BuildRequires: pkgconfig(blkid), pkgconfig(uuid)

%description
bcache is a linux kernel block layer cache. It allows one or more fast disk drivers,
such as flash-based solid state drives (SSDs) to act as a cache for one or more slower
hard disk drives.
bcache-tools contains the userspace tools required for bcache.

%prep
%autosetup -p1

%build
CFLAGS="$CFLAGS -fstack-protector-strong -fPIE -pie -fPIC -D_FORTIFY_SOURCE=2"
export CFLAGS
%make_build all

%install
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}/usr/sbin
mkdir -p %{buildroot}/lib/udev
mkdir -p %{buildroot}/lib/udev/rules.d
mkdir -p %{buildroot}/%{_mandir}/man8
%make_install

%files
%{_sbindir}/make-bcache
%{_sbindir}/bcache
%{_sbindir}/bcache-super-show
/lib/udev/rules.d/69-bcache.rules
/lib/udev/probe-bcache
/lib/udev/bcache-register
/lib/udev/bcache-export-cached
/lib/dracut/modules.d/90bcache/module-setup.sh
/usr/lib/initcpio/install/bcache
/usr/share/initramfs-tools/hooks/bcache
%{_mandir}/man8/*.8*
%license COPYING

%changelog
* Mon Nov 4 2024 liequan che <cheliequan@inspur.com> - 1.1-4
- add bcache-export-cached to export CACHED_UUID and CACHED_LABEL,so that 69-bcache.rules
udev rules can correctly create uuid and label links for bcache devices

* Tue Mar 7 2023 Weifeng Su <suweifeng1@huawei.com> - 1.1-3
- add safe compilation options PIE/pie/PIC/FS

* Mon Sep 6 2021 caodongxia <caodongxia@huawei.com> - 1.1-2
- add safe compilation options fstack-protector-strong

* Mon Jan 6 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 1.1-1
- init bcache-tools v1.1
